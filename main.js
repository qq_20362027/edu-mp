import App from './App'

// #ifndef VUE3
import Vue from 'vue'
import store from './store'

import './uni.promisify.adaptor'
import uView from 'uview-ui'

Vue.config.productionTip = false
App.mpType = 'app'

// 自定义组件
import topNav from '@/components/common/topNav/index'
import menuNav from '@/components/common/menuNav/index'
import search from '@/components/common/search/index'
import operationbtn from '@/components/common/operationBtn/index'
import pay from '@/components/common/pay/index'

Vue.component('topNav', topNav); // 顶部导航栏
Vue.component('menuNav', menuNav); // 底部导航栏
Vue.component('search', search); // 筛选组件
Vue.component('operationbtn', operationbtn); // 底部按钮组件
Vue.component('pay', pay); // 支付组件


import common from '@/static/js/common'
// 公共js
Vue.prototype.common = common; // 公共js

/**
 * 过滤器
 */
//普查筛查任务进度
Vue.filter('taskPrgFilter', function (value) {
  const dic = {
    '0': '未开始',
    '1': '进行中',
    '2': '已失效',
  };
  if (dic.hasOwnProperty(value)) {
    return dic[value];
  }
  return '';
});

//团测记录--测评状态
Vue.filter('testStatusFilter', function (value) {
  const dic = {
    '0': '未开始',
    '1': '未完成',
    '2': '已完成',
  };
  if (dic.hasOwnProperty(value)) {
    return dic[value];
  }
  return '';
});

//团测记录--测评结果
Vue.filter('testResultFilter', function (value) {
  const dic = {
    '0': '正常',
    '1': '关爱',
    '2': '异常',
  };
  if (dic.hasOwnProperty(value)) {
    return dic[value];
  }
  return '';
});

Vue.use(uView)

uni.$u.config.unit = 'rpx'

const app = new Vue({
  ...App,
  store,
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif