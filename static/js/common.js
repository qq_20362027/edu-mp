/*
 * @Author: luocaisen
 * @Date: 2024-10-01 21:27:55
 * @Last Modified by: luocaisen
 * @Last Modified time: 2024-10-17 22:43:44
 * @Description 公共函数
 */
const common = {
  // 获取设备信息
  getSysInfo() {
    const systemInfo = wx.getSystemInfoSync(); // 获取设备信息
    console.log(systemInfo);
    let safeAreaInsetsTop = systemInfo.safeAreaInsets
        ? systemInfo.safeAreaInsets.top
        : 0,
      safeAreaInsetsBottom = systemInfo.safeAreaInsets
        ? systemInfo.safeAreaInsets.bottom
        : 0,
      top = systemInfo.statusBarHeight + safeAreaInsetsTop + uni.upx2px(90),
      bottom = safeAreaInsetsBottom + uni.upx2px(100),
      width = systemInfo.screenWidth,
      height = systemInfo.screenHeight;
    return { top, bottom, width, height };
  },
  // 获取随机字符串
  getRandomStr(length = 32, isUpper = true, hasSymbol = false) {
    let characters =
      'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    if (hasSymbol) {
      characters += '!@#$%^&*()_+[]{}|;:,.<>?';
    }
    let result = '';
    const charactersLength = characters.length;
    for (let i = 0; i < length; i++) {
      result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }
    return isUpper ? result.toUpperCase() : result;
  },
};
export default common;
