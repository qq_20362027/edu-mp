// src/store/index.js
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

// 定义状态
const state = {
  user: {},
  menuNav: 'home', // 底部导航选中
  msgNum: 99, // 消息数量
  token: '', // 登录auth
  systemInfo: {}, // 设备信息
};

// 定义改变状态的方法
const mutations = {
  changeMenuNav(state, nav) {
    state.menuNav = nav;
  },
  set_token(state, token) {
    state.token = token;
  },
  set_systemInfo(state, systemInfo) {
    state.systemInfo = systemInfo;
  },
};

// 创建store
export default new Vuex.Store({
  state,
  mutations,
});
