/*
 * @Author: luocaisen
 * @Date: 2024-10-15 22:44:21
 * @Last Modified by: luocaisen
 * @Last Modified time: 2024-10-15 22:50:03
 * @description 我的api
 */
import { request, get, post } from './config.js';

import store from '@/store';

const myApi = {
  // 获取用户信息
  getUserInfo() {
    return get('/wx/getUserInfo');
  },
  // 绑定子女
  bindChildren() {
    return post('/wx/bindChildren');
  },
  // 解绑子女
  unBindChildren() {
    return post('/wx/unBindChildren');
  },
  // 获取机构
  getOrganizational() {
    return get('/wx/getOrganizational');
  },
  // 校验学号
  checkStudentId(studentId) {
    return post('/wx/checkStudentId', { studentId });
  },
};

export default myApi;
