/*
 * @Author: luocaisen
 * @Date: 2024-09-07 14:48:08
 * @Last Modified by: luocaisen
 * @Last Modified time: 2024-10-17 23:10:03
 * @description http请求
 */
import { request, get, post } from './config.js';

const api = {
  // 登录
  login(code) {
    return get('/wx/in', { code });
  },
  // 下单
  placeOrder() {
    return get('/wx/submit', { amount: 555.555 });
  },
  // 解析电话号码
  getPhoneNumber(data) {
    return post('/wx/savePhone', data);
  },
  // 保存用户信息
  saveUserinfo(data) {
    return post('/wx/saveUser', data);
  },
  // 获取消息列表
  getMessage() {
    return get('/wx/getMessage');
  },
  wxPay(amount){
    return get('/wx/payment',{amount});
  }
};

export default api;
