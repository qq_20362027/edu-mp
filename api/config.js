/*
 * @Author: luocaisen
 * @Date: 2024-09-07 14:42:55
 * @Last Modified by: luocaisen
 * @Last Modified time: 2024-10-17 23:01:00
 * @description http请求配置
 */
let headers;
const domain = 'http://47.97.116.47:8001';
// const domain = 'http://47.108.89.251:8001';
// const domain = 'http://localhost:80'


headers = {
  'Content-Type': 'application/json',
};

function _request(config) {
  return new Promise((resolve, reject) => {
    uni.request({
      ...{
        success(r) {
          if (r.statusCode == 200) {
            resolve(r.data);
          } else {
            reject(r);
          }
        },
        error(e) {
          console.error(config.url + ':' + e);
          reject(e);
        },
        fail(ee) {},
        timeout: 60000,
      },
      ...config,
    });
  });
}

/**
 * http请求统一处理
 * @param {*} type 请求类型
 * @param {*} requestConfig 请求参数配置
 * @param {*} useDefaultHeader 是否使用默认header
 * @returns
 */
function httpRequest(type, requestConfig, useDefaultHeader = false) {
  let result = null;
  const config = {
    url: '',
    data: '',
    method: 'get',
    timeout: 120000,
    header: {},
    ...requestConfig,
  };

  config.url = `${domain}${config.url}`;
  const token = uni.getStorageSync('token');
  if (token) {
    config.header['Authorization'] = token;
  }

  switch (type) {
    case 'get':
      config.method = 'get';
      result = _request(config);
      break;
    case 'post':
      config.method = 'post';
      result = _request(config);
      break;
    case 'request':
      result = _request(config);
      break;
    case 'put':
      config.method = 'put';
      result = _request(config);
      break;
    case 'delete':
      config.method = 'delete';
      result = _request(config);
      break;
    default:
      result = _request(config);
      break;
  }
  return result;
}

function request(config, useDefaultHeader) {
  return httpRequest('request', config, useDefaultHeader);
}

function get(url, data) {
  return httpRequest(
    'get',
    {
      url,
      data,
    },
    true
  );
}
/**
 * post json
 * @param {*} url
 * @param {*} data
 * @returns
 */
function post(url, data) {
  return httpRequest(
    'post',
    {
      url,
      data,
    },
    true
  );
}
/**
 * 基本的表单提交 form
 * @param {*} url
 * @param {*} data
 * @returns
 */
function post_form(url, data) {
  return httpRequest(
    'request',
    {
      url: url,
      data: data,
      method: 'POST',
      header: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
    },
    true
  );
}

function put(url, data) {
  return httpRequest(
    'put',
    {
      url,
      data,
    },
    true
  );
}

function del(url, data) {
  return httpRequest(
    'delete',
    {
      url,
      data,
    },
    true
  );
}

export { request, get, post, put, del, post_form };
