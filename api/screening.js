/* 
* @Author: luocaisen 
* @Date: 2024-10-15 22:38:00 
* @Last Modified by:   luocaisen 
* @Last Modified time: 2024-10-15 22:38:00 
* @description 普查筛查api
*/
import { request, get, post } from './config.js';

import store from '@/store';

// 获取登录auth
function getAuth() {
  return store.state.token;
}

const screeningApi = {
  // 获取团测列表
  getScreeningList(parms, pagination) {
    /*
        "faqiren": "string",
          "pageIndex": 0,
          "pageSize": 0,
          "renwujindu": "string",
          "tuancename": "string",
          "tuancezhuangtai": "string",
          "学年": "string"
          */
    let data = {
      ...{
        Authorization: getAuth(),
        pageIndex: 1,
        pageSize: 5,
      },
      ...parms,
      ...pagination,
    };
    return post('/wx/getWxTuanCeRes', data);
  },
  // 获取试题详情
  getTestDetail(parms) {},
  // 提交试题
  submitTest(data) {},
};

export default screeningApi;
