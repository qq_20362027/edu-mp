const recordJson = {
    code: 200,
    data: [{
        num: '01',
        name: '0909测评',
        year: '2024',
        promoter: 'admin',
        Reviewed: 2,
        shouldReview: 54,
        reviewAbnormal: 2,
        startTime: '2024-09-09 14:02:25',
        taskProgress: 1
    },
    {
        num: '02',
        name: '2121',
        year: '2024',
        promoter: 'admin',
        Reviewed: 6,
        shouldReview: 33,
        reviewAbnormal: 6,
        startTime: '2024-09-06 13::48:11',
        taskProgress: 2
    },
    {
        num: '03',
        name: '新学期心理测评',
        year: '2024',
        promoter: 'admin',
        Reviewed: 0,
        shouldReview: 105,
        reviewAbnormal: 0,
        startTime: '2024-08-13 17:51:22',
        taskProgress: 0
    },
    {
        num: '04',
        name: '夏季心理测评',
        year: '2024',
        promoter: 'admin',
        Reviewed: 0,
        shouldReview: 105,
        reviewAbnormal: 0,
        startTime: '2024-08-13 17:49:46',
        taskProgress: 2
    },],
    pagination: {
        page: 1,
        pageSize: 10,
        total: 4,
    }
};
const testRecordJson = {
    code: 200,
    data: [
        {
            num: '01',
            name: '李泽',
            studentsId: '10000014',
            testName: '21',
            testDate: '2024-09-11',
            year: 2024,
            class: '高二(1)班',
            tableNum: 1,
            testStatus: '2',
            monitorStatus: '1',//监测异常
            testResult: '1',//测评结果            
        },
        {
            num: '02',
            name: '11',
            studentsId: '10000014',
            testName: '21',
            testDate: '2024-09-11',
            year: 2024,
            class: '高二(1)班',
            tableNum: 1,
            testStatus: '2',
            monitorStatus: '0',//监测异常
            testResult: '1',//测评结果            
        },
        {
            num: '03',
            name: '李泽',
            studentsId: '10000014',
            testName: '21',
            testDate: '2024-09-11',
            year: 2024,
            class: '高二(1)班',
            tableNum: 1,
            testStatus: '2',
            monitorStatus: '1',//监测异常
            testResult: '2',//测评结果            
        },
        {
            num: '04',
            name: '李泽',
            studentsId: '10000014',
            testName: '21',
            testDate: '2024-09-11',
            year: 2024,
            class: '高二(1)班',
            tableNum: 1,
            testStatus: '2',
            monitorStatus: '1',//监测异常
            testResult: '0',//测评结果            
        },
        {
            num: '05',
            name: '李泽',
            studentsId: '10000014',
            testName: '21',
            testDate: '2024-09-11',
            year: 2024,
            class: '高二(1)班',
            tableNum: 1,
            testStatus: '2',
            monitorStatus: '0',//监测异常
            testResult: '1',//测评结果            
        },
    ],
    pagination: {
        page: 1,
        pageSize: 10,
        total: 4,
    }
};

const messageJson = {
    code: 200,
    data: [
        {
            id: '1',
            type: '预警信息',
            time: '2024-05-25 14:52:00',
            content: '消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息'
        },
        {
            id: '2',
            type: '预警信息',
            time: '2024-05-24 14:52:00',
            content: '消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息'
        },
        {
            id: '3',
            type: '预警信息',
            time: '2024-05-23 14:52:00',
            content: '消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息消息'
        },
    ],
    pagination: {
        page: 1,
        pageSize: 10,
        total: 4,
    }
};

const recordJson1 = {
    code: 200,
    data: [],
    pagination: {
        page: 1,
        pageSize: 10,
        total: 4,
    }
};
export {
    recordJson,
    testRecordJson,
    messageJson,
};